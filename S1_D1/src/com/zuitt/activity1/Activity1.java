package com.zuitt.activity1;
import java.util.Scanner;
public class Activity1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("First Name: ");
        String firstName = scanner.nextLine();

        System.out.println("Last Name: ");
        String lastName = scanner.nextLine();

        System.out.println("First Subject Grade: ");
        String gradeA = scanner.nextLine();
        double a = Double.parseDouble(gradeA);

        System.out.println("Second Subject Grade: ");
        String gradeB = scanner.nextLine();
        double b = Double.parseDouble(gradeB);

        System.out.println("Third Subject Grade: ");
        String gradeC = scanner.nextLine();
        double c = Double.parseDouble(gradeC);

        System.out.println("Good day, "+firstName+" "+lastName+".");
        System.out.println("Your grade average is: "+((a+b+c)/3));
    }

}
