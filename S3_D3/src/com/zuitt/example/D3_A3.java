package com.zuitt.example;
import java.util.Scanner;

public class D3_A3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Input an integer whose factorial will be computed: ");
        int num = in.nextInt();
        int factorial = 1;
        for(int i = 1; i <= num; i++){
            factorial *=i;
        }
        System.out.println("The factorial of "+num+" is "+ factorial);

        for(int r = 1; r <= 5; r++){
            System.out.println();
            for(int c = 1; c <= r; c++){
                System.out.print("*");
            }
        }
    }
}
