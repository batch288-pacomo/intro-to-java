package com.zuitt.example;
import java.util.Scanner;
public class ExceptionHandling {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Input a number: ");
        int num = 0;
        //nextInt(),nextDouble(),nextLine()
        try{
            num = input.nextInt();
        } catch(Exception e){
            System.out.println("Invalid input");
            e.printStackTrace();
        }
        System.out.println("You have entered: "+num);
    }
}
