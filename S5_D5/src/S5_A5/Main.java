package S5_A5;

public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact("John Doe", "1234567890", "Quezon City");
        Contact contact2 = new Contact("Jane Doe", "0987654321", "Makati City");

        phonebook.addContact(contact1);
        phonebook.addContact(contact2);

        if (phonebook.isEmpty()) {
            System.out.println("Phonebook is empty");
        } else {
            System.out.println("Contacts in the phonebook:");
            phonebook.displayContacts();
        }
    }
}