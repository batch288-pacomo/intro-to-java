package com.zuitt.example;

public interface Actions {
    public void sleep();
    public void run();
    public void turn_on();
    public void turn_off();

}
