//Activity

INSERT INTO users (id, email, password, datetime_created) VALUES (1, "johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00");
INSERT INTO users (id, email, password, datetime_created) VALUES (2, "juandelacruz@gmail.com", "passwordB", "2021-01-01 02:00:00");
INSERT INTO users (id, email, password, datetime_created) VALUES (3, "janesmith@gmail.com", "passwordC", "2021-01-01 03:00:00");
INSERT INTO users (id, email, password, datetime_created) VALUES (4, "mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00");
INSERT INTO users (id, email, password, datetime_created) VALUES (5, "johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");

INSERT INTO author (id, name) VALUES(1, "A");
INSERT INTO author (id, name) VALUES(2, "B");
INSERT INTO author (id, name) VALUES(3, "C");
INSERT INTO author (id, name) VALUES(4, "D");

INSERT INTO posts (id, title, content, datetime_created, author_id) VALUES (1, "First Code", "Hello World!", "2021-01-02 01:00:00", 1);
INSERT INTO posts (id, title, content, datetime_created, author_id) VALUES (2, "Second Code", "Hello Earth!", "2021-01-02 02:00:00", 2);
INSERT INTO posts (id, title, content, datetime_created, author_id) VALUES (3, "Third Code", "Welcome to Mars!", "2021-01-02 03:00:00", 3);
INSERT INTO posts (id, title, content, datetime_created, author_id) VALUES (4, "Fourth Code", "Bye bye solar system!", "2021-01-02 04:00:00", 4);

SELECT * FROM author WHERE id = 1;
SELECT email, datetime_created FROM users;
UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id = 2;
DELETE FROM users WHERE email = "johndoe@gmail.com";
