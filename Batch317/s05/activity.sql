SELECT customerName FROM customers WHERE country = "Philippines";
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";
SELECT productName, MSRP FROM products WHERE productName = "The Titanic";
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";
SELECT customerName FROM customers WHERE state IS NULL;
SELECT firstName, lastName, email FROM employees WHERE firstName = "Steve" && lastName = "Patterson";
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" && creditLimit > 3000;
SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";
SELECT DISTINCT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";
SELECT DISTINCT country FROM customers;
SELECT DISTINCT status FROM orders;
SELECT customerName, country FROM customers WHERE country IN ("USA","France","Canada");

SELECT employees.firstName, employees.lastName, offices.city FROM employees
JOIN offices ON employees.officeCode = offices.officeCode
WHERE offices.city = "Tokyo";

SELECT customers.customerName FROM customers
JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
WHERE employees.lastName = "Thompson" && employees.firstName = "Leslie";

SELECT products.productName, customers.customerName 
FROM customers
JOIN orders ON customers.customerNumber = orders.customerNumber
JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
JOIN products ON orderdetails.productCode = products.productCode
WHERE customers.customerName = 'Baane Mini Imports';

SELECT employees.firstName, employees.lastName, customers.customerName, offices.country
FROM customers
JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
JOIN offices ON employees.officeCode = offices.officeCode
WHERE customers.country = offices.country;

SELECT productName, quantityInStock FROM products WHERE productLine = "Planes" && quantityInStock < 1000;
SELECT customerName, phone FROM customers WHERE phone LIKE "+81%";