package D4_A4;

public class Main {
    public static void main(String[] args) {
        User user1 = new User();
        Course course1 = new Course();

        user1.setName("Terence Gaffud");
        user1.setAge(25);
        user1.setEmail("tgaff@mail.com");
        user1.setAddress("Quezon City");

        course1.setName("MACQ004");
        course1.setDescription("An introduction to Java for career-shifters");

        System.out.println("Hi! I'm "+user1.getName()+". I'm "+user1.getAge()+" years old. You can reach me via my email: "+user1.getEmail()+". When I'm off work, I can be found at my house in "+user1.getAddress());

        System.out.println("Welcome to the course "+course1.getName()+". This course can be described as "+course1.getDescription()+". Your instructor for this course is Sir "+user1.getName()+". Enjoy!");

    }
}
