package com.zuitt.example;

public class Main {
    public static void main(String[] args) {
        Car car1 = new Car();
//        System.out.println(car1.brand);
//        System.out.println(car1.make);
//        System.out.println(car1.price);
//        car1.make = "Veyron";
//        car1.brand = "Bugatti";
//        car1.price = 200000;
//        System.out.println(car1.brand);
//        System.out.println(car1.make);
//        System.out.println(car1.price);
        //Instance -  an object created from a class and each instance should be independent of each other
        Car car2 = new Car();
        Car car3 = new Car();
         /*
            Mini-Activity:

            Create two new instances of the Car class and save it in a variable called car2 and car3 respectively.
            Access the properties of the instance and update its values.
                make = String
                brand = String
                price = int
             You can come up with your own values.
             Print the values of each property of the instance.
        */
        Driver driver1 = new Driver("Alejandro", 25);
//        System.out.println(driver1.name);

        car1.start();
        car2.start();
        car3.start();

        //property getters
        System.out.println(car1.getMake());
        System.out.println(car2.getMake());

        //property setters
        car1.setMake("Veyron");
        System.out.println(car1.getMake());

        car2.setMake("Innova");
        System.out.println(car2.getMake());

        //carDriver Getter
        System.out.println(car1.getCarDriver().getName());

        Driver newDriver = new Driver("Antonio", 21);
        car1.setCarDriver(newDriver);

//        Get name of new carDriver
        System.out.println(car1.getCarDriver().getName());
        System.out.println(car1.getCarDriver().getAge());

        System.out.println(car1.getCarDriverName());
        /*
         * Mini-Activity
         * Create a new class called Animal with the following attributes
         * name - string
         * color - string
         *
         * Add constructors, getters and setters for the class.
         * */

        Animal animal1 = new Animal("Clifford", "Red");
        animal1.call();

        Dog dog1 =  new Dog();
        System.out.println(dog1.getName());
        dog1.call();
        dog1.setName("Hachiko");
        System.out.println(dog1.getName());
        dog1.call();
        dog1.setColor("Brown");
        System.out.println(dog1.getColor());

        Dog dog2 =  new Dog("Mike", "Dark Brown", "Corgi");
        dog2.call();
        System.out.println(dog2.getName());

        System.out.println(dog2.getDogBreed());
        dog2.greet();
    }
}


