package com.zuitt.example;
import java.util.Scanner;
public class S2_A1 {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Input year to be check if a leap year.");

        int year = Integer.parseInt(scan.nextLine());

        if(year%4 ==0){
            if(year%100 !=0 || year%400 == 0){
                System.out.println(year+" is a leap year");
            }else{
                System.out.println(year+" is NOT a leap year");
            }
        }else{
            System.out.println(year+" is NOT a leap year");
        }



    }
}
