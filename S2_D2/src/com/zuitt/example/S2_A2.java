package com.zuitt.example;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class S2_A2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int[] primeNumber = {2,3,5,7,11};
        System.out.println("Enter number 0 to 4: ");
        int i = Integer.parseInt(scan.nextLine());
        System.out.println("The first prime number is: "+primeNumber[i]);

        ArrayList<String> names = new ArrayList<String>();
        names.add("John");
        names.add("Jane");
        names.add("Chloe");
        names.add("Zoey");
        System.out.println("My friends are: "+names);

        HashMap<String, String>items = new HashMap<String, String>();
        items.put("toothpaste","15");
        items.put("toothbrush","20");
        items.put("soap","12");

        System.out.println("Our current inventory consists of: "+items);
    }
}
